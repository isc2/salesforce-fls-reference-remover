#!/usr/bin/env python3

import argparse
import glob
import json
import re


class ReferenceRemover(object):
    """Removes contents of FLS profiles and permission sets based upon prefixes and regular expressions."""

    DEFAULT_CONFIG_PATH = "./reference_remover.json"

    def run(self):
        """Runs the script, cleaning all files based upon provided options."""
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "source_dir",
            help="The base directory containing Salesforce metadata to operate on with NO trailing slash. e.g. ./src",
        )
        parser.add_argument(
            "--config",
            help="Path to the JSON config to use. If not specified, defaults to ./reference_remover.json.",
        )
        args = parser.parse_args()

        # Override default config if specified as a flag.
        config_path = self.DEFAULT_CONFIG_PATH

        if args.config:
            print('Using config specified at "{0}".'.format(args.config))
            config_path = args.config

        # Load the JSON config.
        with open(config_path, "r") as f:
            config = json.load(f)

        # Remove portions of files.
        paths = config["PathsToProcess"]

        for path in paths:
            combined_path = args.source_dir + path
            print(
                'Beggining directory cleaning following pattern "{0}".'.format(
                    combined_path
                )
            )
            for file_name in glob.glob(combined_path):
                self._clean_file(file_name, config)

    def _clean_file(self, file_name, config):
        """Cleans individual files."""

        # Opens the file with read/write access.
        file_to_process = open(file_name, "r+")

        print('Cleaning file "{0}".'.format(file_name))

        file_contents = file_to_process.read()

        matchers = config["Matchers"]
        regexes = config["Regexes"]

        for matcher in matchers:
            for regex in regexes:
                regex_with_filled_matcher = regex.format(matcher)
                file_contents = re.sub(regex_with_filled_matcher, "", file_contents)

        # Bring us back to the beginning of the file, write the output, remove what was there.
        file_to_process.seek(0)
        file_to_process.write(file_contents)
        file_to_process.truncate()
        file_to_process.close()


if __name__ == "__main__":
    ReferenceRemover().run()
