# (ISC)² Salesforce FLS Metadata Reference Remover

 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

This tool provides an automated way to remove references from Salesforce Profile and Permission set metadata based upon configurable prefix patterns.

This can be particularly useful in a scenario where your metadata is stored in source control and you need to remove references to specific managed package namespaces from all profiles and permission sets.

## Pre-requisites

1. Python 3.x. If you're using Mac OS Catalina, you're good to go. Anything else, you'll possibly need to set it up.
2. Salesforce metadata should always be stored in a secure source control repository. ⚠️ **This tool makes destructive changes to files.** ⚠️ It is up to you to ensure that you have adequate protections in place to prevent undesired changes. You'll want these files tracked in source control _before_ using this tool to ensure you can revert them to their prior state if needed.
3. Knowledge of a CLI environment from which commands may be run.

## Basic Usage

1. Use Git to clone this repository to the directory containing your Salesforce project.
2. Update the prefixes which you wish to remove in the `Matchers` section of the `reference_remove.json` file following the provided examples. You can add as many as you'd like, the more there are, the longer the tool will take to run.
3. Run the tool, specifying the path to the root `src` directory containing your metadata. e.g. `./reference_remover.py ./src` if you've cloned this repository to the directory above `src`.
4. After reviewing the changes, commit the changes back to your repository if you are happy with the outcome. If not, just reset the files to their prior state.

## Additional Options

```
usage: reference_remover.py [-h] [--config CONFIG] source_dir

positional arguments:
  source_dir       The base directory containing Salesforce metadata to
                   operate on with NO trailing slash. e.g. ./src

optional arguments:
  -h, --help       show this help message and exit
  --config CONFIG  Path to the JSON config to use. If not specified, defaults
                   to ./reference_remover.json.
```

You may also adjust the regular expressions, directory paths, and prefixes in the provided `reference_remover.json` file as you see fit.

## License

MIT. See LICENSE.md for full information.